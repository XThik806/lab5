# Описати функцію ArcSin1(X) дійсного типу (X - дійсне , задано в градусах). Скласти 
# програму розв’язку рівняння sin(ax+b)=c.

import math

def ArcSin1(X):
    return math.asin(math.radians(X))


A = float(input("Введіть значення для A: "))
B = float(input("Введіть значення для B: "))
C = float(input("Введіть значення для C: "))

X = (ArcSin1(C) - B) / A

print(F"Значення X = {X:.2f}")