# Описати функцію IsPrime (N) логічного типу, яка повертає True, якщо цілий 
# параметр N (> 1) є простим числом, і False в іншому випадку (число, більше 1, 
# називається простим, якщо воно не має позитивних дільників, крім 1 і самого себе). 
# Дано набір з 10 цілих чисел, більших 1. За допомогою функції IsPrime знайти 
# кількість простих чисел в даному наборі

def IsPrime(N):
    
    if N <= 1: return False

    for i in range (2, int(N**0.5)+1):
        if N % i == 0: return False

    return True

list = []

for i in range (10):
    number = int(input("Введіть число що більше за 1: "))
    if number < 1:
        while(number < 1):
            number = int(input("Число має бути більше за 1, спробуй знову: "))
    list.append(number)

for i in list:
    if IsPrime(i): print(f"Число {i} є простим")
    else: print(f"Число {i} НЕ є простим")