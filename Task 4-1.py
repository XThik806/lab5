# Описати функцію Power1 (A, B) дійсного типу, яка знаходить величину AB за 
# формулою AB = exp (B ln (A)) (параметри A і B - дійсні). У разі нульового або 
# негативного параметра A функція повертає 0. З допомогою цієї функції знайти 
# степені AP, BP, CP, якщо дано числа P, A, B, C.

import math

def Power1(A, B):
    B = math.exp(math.log(A))
    if B <= 0: return 0
    return B

P = int(input("Введіть значення для P: "))
A = int(input("Введіть значення для A: "))
B = int(input("Введіть значення для B: "))
C = int(input("Введіть значення для C: "))

print(Power1(A, P))
print(Power1(B, P))
print(Power1(C, P))